require("dotenv").config();
const fs = require("fs");
const discord = require("discord.js");
const { Sequelize } = require("sequelize");
const { log, warn, error, debug } = require("./lib/logger.js");
const commands = require("./lib/commands");
const config = require("./lib/config");
const sequelize = require("./lib/db.js");

async function DMAdmin(message) {
	let admin = await client.users.fetch(config.admin);
	let dm = admin.dmChannel ?? (await admin.createDM());
	return await dm.send(message);
}

const client = new discord.Client({
	intents: ["Guilds", "GuildMessages"],
});

client.on("ready", async () => {
	log("Bot ready!");
});

// Command handling
client.on("interactionCreate", async (interaction) => {
	if (!interaction.isChatInputCommand()) return;
	let command = commands.find(
		(c) => c.slashCommand.name === interaction.commandName
	);
	if (!command) {
		interaction.reply(
			`Command not found! Notify <@${config.admin}> about this.`
		);
		DMAdmin(
			`Command ${interaction.commandName} not found. Run \`npm run registerSlashCommands\`.`
		);
	} else {
		debug(
			`User ${interaction.user.tag} (${interaction.user.id}) ran /${interaction.commandName}`
		);
		try {
			await command.execute(interaction, commands);
		} catch (err) {
			error(`Command ${interaction.commandName} failed to execute`);
			DMAdmin(`Command ${interaction.commandName} failed to execute`);
			error(err);
		}
	}
});

// Connect to DB, Enable WAL and start the client
sequelize
	.authenticate()
	.then(() => sequelize.query("PRAGMA journal_mode=WAL;"))
	.then(() => sequelize.sync())
	.then(() => client.login(process.env.TOKEN))
	.catch(error);

process.on("beforeExit", () => {});

log("Starting...");
