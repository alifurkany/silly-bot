const discord = require("discord.js");
const nineteeneightyfour = Date.UTC(1984, 0, 1);
const config = require("../lib/config");

module.exports = {
	slashCommand: new discord.SlashCommandBuilder()
		.setName("help")
		.setDescription("See help for the bot")
		.addStringOption((o) =>
			o.setName("command").setDescription("Command to see help for")
		),
	/**
	 * @param {discord.ChatInputCommandInteraction} interaction
	 * @param {import("../lib/commands")} commands
	 */
	async execute(interaction, commands) {
		let embed = new discord.EmbedBuilder().setTimestamp(nineteeneightyfour);
		let commandOption = interaction.options.getString("command", false);
		if (commandOption) {
			let command = commands.find(
				(c) => c.slashCommand.name === commandOption
			);
			if (!command)
				return interaction.reply(`Command ${commandOption} not found.`);
			embed
				.setTitle(command.slashCommand.name)
				.setDescription(command.slashCommand.description);
		} else {
			embed
				.setTitle(`${config.name} help`)
				.setDescription(config.description)
				.addFields(
					...commands.map(({ slashCommand: command }) => ({
						name: command.name,
						value: command.description,
					}))
				);
		}
		interaction.reply({ embeds: [embed] });
	},
};
