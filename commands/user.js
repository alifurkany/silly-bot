const discord = require("discord.js");
const sequelize = require("../lib/db");

module.exports = {
	slashCommand: new discord.SlashCommandBuilder()
		.setName("user")
		.setDescription("Get information about yourself"),
	/**
	 * @param {discord.ChatInputCommandInteraction} interaction
	 * @param {import("../lib/commands")} commands
	 */
	async execute(interaction, commands) {
		let [user, created] = await sequelize.models.User.findOrCreate({
			where: {
				id: interaction.user.id,
			},
		});
		interaction.reply(
			(created
				? `Added you to the database!`
				: `First seen at <t:${Math.floor(
						user.firstSeen.getTime() / 1000
				  )}>.`) +
				` ${await sequelize.models.User.count()} user(s) in database.`
		);
	},
};
