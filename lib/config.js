const { log, debug, warn, error } = require("./logger");

const config = {
	name: "Silly Bot",
	description: "Simple Discord.js Bot",
	admin: "276363003270791168",
};

debug("Loading config")
let configFile;
try {
	configFile = require("../config.json");
} catch (e) {
	warn(
		"Config file does not exist, using defaults. See README.md for more information."
	);
}

for (let key in config) {
	config[key] = configFile[key] ?? config[key];
}
debug("Loaded config")

module.exports = config;
