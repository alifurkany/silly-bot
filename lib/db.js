const { debug } = require("./logger");
const { Sequelize, DataTypes } = require("sequelize");

// Database
const sequelize = new Sequelize(process.env.DB_PATH, { logging: debug });

// Models
sequelize.define("User", {
	id: {
		type: DataTypes.STRING,
		primaryKey: true,
	},
	firstSeen: {
		type: DataTypes.DATE,
		defaultValue: () => new Date(),
	},
});

module.exports = sequelize;
