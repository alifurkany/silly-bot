const HelpCommand = require("../commands/help");
const UserCommand = require("../commands/user");

module.exports = [HelpCommand, UserCommand];
