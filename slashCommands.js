require("dotenv").config();
const { REST, Routes } = require("discord.js");
const { log, error } = require("./lib/logger.js");

const commands = Object.values(require("./lib/commands")).map(
	(c) => c.slashCommand
);

// Construct and prepare an instance of the REST module
const rest = new REST().setToken(process.env.TOKEN);

// and deploy your commands!
(async () => {
	try {
		log(`Started refreshing ${commands.length} application (/) commands.`);

		// The put method is used to fully refresh all commands in the guild with the current set
		const data = await rest.put(
			Routes.applicationCommands(process.env.CLIENT_ID),
			{ body: commands }
		);

		log(`Successfully reloaded ${data.length} application (/) commands.`);
	} catch (err) {
		// And of course, make sure you catch and log any errors!
		error(err);
	}
})();
