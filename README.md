# Silly Bot

Simple Discord.js bot

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements
subsection.

## Configuration

Put your secrets ([database connection URI](https://sequelize.org/docs/v6/getting-started/#connecting-to-a-database) and Discord bot token) in a file called `.env`, formatted in a TOML-like way. Currently only intended to be used with SQLite, but can be easily edited to use other databases.

```
CLIENT_ID=1852889158534926
TOKEN=MTA2NzQ1MDc2ODU5NDYwNTc4.AskdRr.evAaEWtWNwBt8e5oFD3b57ZSqpj
DB_PATH=sqlite://silly.db
```

The rest of the configuration is done in `config.json`. It will be generated with defaults when you run the bot if it does not exist.

```json
{
	"name": "Silly Bot",
	"description": "Simple Discord.js Bot",
	"admin": "276363003270791168"
}
```

Run `npm run registerSlashCommands` to get slash commands.

## Usage

Start the bot with `npm start` or use nodemon with `npm run nodemon`

## Contributing

Make a merge request, I'll review it and probably merge if it's not a virus or licensed under something less permissive.

## Authors and acknowledgment

Me

## License

This project is licensed under the MIT license. See [LICENSE](LICENSE) for mre information.
